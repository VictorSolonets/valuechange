//
//  Date.swift
//  ValueChange
//
//  Created by VictorSolo on 1/5/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Foundation

extension Date {
    
    static func getTimeInterval(from date:Date) -> Date {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "dd.MM.yyyy";
        let dateString = dateFormatter.string(from: date)
        let newDate = dateFormatter.date(from: dateString)
        return newDate!
    }
    
}
