//
//  UIViewController.swift
//  ValueChange
//
//  Created by VictorSolo on 1/6/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            CustomLoadingHUD.hideHUD(in: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            CustomLoadingHUD.showHUD(in: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
        })
    }
    
}
