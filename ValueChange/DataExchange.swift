//
//  DataExchange.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation

class DataExchange {
    
    var baseCCY: String!
    var exchangerateCCY: String!
    var saleValue: String!
    var buyValue: String!
    
    init (baseCCY: String, exchangerateCCY: String, saleValue: String, buyValue: String) {
        self.baseCCY = baseCCY
        self.exchangerateCCY = exchangerateCCY
        self.saleValue = saleValue
        self.buyValue = buyValue
    }
}
