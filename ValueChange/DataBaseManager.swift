//
//  DateBaseManager.swift
//  ValueChange
//
//  Created by VictorSolo on 1/5/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Foundation
import CoreData

class DataBaseManager {
    
    private var mainObjectContext: NSManagedObjectContext!
    
    static let instance = DataBaseManager()
    
    private init() {
        mainObjectContext = nil
    }
    
    static let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    private let storeURL = documentsDirectory.appendingPathComponent("CurrencuModel.model")
    
    public func getMainContext() -> NSManagedObjectContext {
        if self.mainObjectContext == nil {
            let bundles = [Bundle.init(for: RateExchange.self)]
            guard let model = NSManagedObjectModel.mergedModel(from: bundles) else { fatalError("model not found") }
            let psc = NSPersistentStoreCoordinator(managedObjectModel: model)
            try! psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            context.persistentStoreCoordinator = psc
            self.mainObjectContext = context
            return context
        } else {
            return self.mainObjectContext
        }
    }
    
    public func storeRateExchange (dateForStore:DataCurrency) {
        
        guard let rateExchange = NSEntityDescription.insertNewObject(forEntityName: "RateExchange", into: getMainContext()) as? RateExchange else {
            fatalError("Wrong object type")
        }
        
        rateExchange.date = dateForStore.date
        rateExchange.baseCurrency = dateForStore.baseCurrency
        rateExchange.currency = dateForStore.currency
        rateExchange.purchaseRate = dateForStore.purchaseRate ?? 0.0
        rateExchange.purchaseRateNB = dateForStore.purchaseRateNB ?? 0.0
        rateExchange.saleRate = dateForStore.saleRate ?? 0.0
        rateExchange.saleRateNB = dateForStore.saleRateNB ?? 0.0
        
        try! getMainContext().save()
        print("saved!")
    }
    
    public func getRateExchange(from dateToGet:NSDate, forGraphic:Bool) -> [DataCurrency] {
        var resultDataArray = [DataCurrency]()
        let fetchRequest: NSFetchRequest<RateExchange> = RateExchange.fetchRequest()
        let sortDescription = NSSortDescriptor.init(key: "date", ascending: true)
        fetchRequest.predicate = forGraphic ?
                                            NSPredicate(format: "date > %@ && (currency == %@ || currency == %@)", dateToGet, "USD", "EUR") :
                                            NSPredicate(format: "date == %@", dateToGet)
        fetchRequest.sortDescriptors = [sortDescription]
        fetchRequest.fetchBatchSize = 30
        
        do {
            let searchResults = try getMainContext().fetch(fetchRequest)
            
            print ("num of results = \(searchResults.count)")
            
            for rateExchange in searchResults as [NSManagedObject] {
                let resultData = DataCurrency.init(date: rateExchange.value(forKey: "date") as! NSDate,
                                                   baseCurrency: rateExchange.value(forKey: "baseCurrency") as! String?,
                                                   currency: rateExchange.value(forKey: "currency") as! String?,
                                                   purchaseRate: rateExchange.value(forKey: "purchaseRate") as! Double?,
                                                   purchaseRateNB: rateExchange.value(forKey: "purchaseRateNB") as! Double?,
                                                   saleRate: rateExchange.value(forKey: "saleRate") as! Double?,
                                                   saleRateNB: rateExchange.value(forKey: "saleRateNB") as! Double?)
                resultDataArray.append(resultData)
            }
        } catch {
            print("Error with request: \(error)")
        }
        print("\(resultDataArray)")
        return resultDataArray
    }
}
