//
//  DataHistoryExchange.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation

class DataHistoryExchange {
    
    var baseCurrency: String!
    var currency: String!
    var purchaseRate: Float!
    var purchaseRateNB: Float!
    var saleRate: Float!
    var saleRateNB: Float!
    
    init (baseCurrency: String?, currency: String?, purchaseRate: Float?, purchaseRateNB: Float?, saleRate: Float?, saleRateNB: Float?) {
        self.baseCurrency = baseCurrency
        self.currency = currency
        self.purchaseRate = purchaseRate
        self.purchaseRateNB = purchaseRateNB
        self.saleRate = saleRate
        self.saleRateNB = saleRateNB
    }
}
