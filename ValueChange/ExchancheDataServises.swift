//
//  ExchangeAPI.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation
import Alamofire

class ExchancheDataServises {
    
    static let instance = ExchancheDataServises()
    
    private init() { }
    
    func downloadInCashValues(inCashValue:Bool, completed : @escaping DownloadCompleate) {
        
        let URL = inCashValue ? IN_CASH_VALUE_CHANGE_URL : NON_CASH_VALUE_CHANGE_URL
        
        var baseCCY = "", exchangerateCCY = ""
        var saleValue = 0.0, buyValue = 0.0
        
        request(URL).responseJSON { (response) in
            print(response)
            var resultArray:Array = [DataCurrency]()
            if let JSON = response.result.value as? [[String : Any]] {
                for i in 0..<JSON.count {
                    if let baseCCYValue = JSON[i]["ccy"] as? String {
                        baseCCY = baseCCYValue
                    }
                    if let exchangerateCCYValue = JSON[i]["base_ccy"] as? String {
                        exchangerateCCY = exchangerateCCYValue
                    }
                    if let JSONBuyValue = JSON[i]["buy"] as? String {
                        buyValue = Double(JSONBuyValue)!
                    }
                    if let JSONSaleValue = JSON[i]["sale"] as? String {
                        saleValue = Double(JSONSaleValue)!
                    }
                    if (exchangerateCCY != "" && baseCCY != "" && buyValue != 0.0 && saleValue != 0.0) {
                        let dataHistoryGraphic = DataCurrency.init(date:Date.getTimeInterval(from: Date.init()) as NSDate, baseCurrency: exchangerateCCY, currency: baseCCY, purchaseRate: buyValue, purchaseRateNB: buyValue, saleRate: saleValue, saleRateNB: saleValue)
                        resultArray.append(dataHistoryGraphic)
                    }
                }
            }
            if (resultArray.count) > 0 {
                let resultData = NSKeyedArchiver.archivedData(withRootObject: resultArray)
                UserDefaults.standard.set(resultData, forKey: "TodayExchange")
                UserDefaults.standard.set(Date.getTimeInterval(from: Date.init()), forKey: "TodayDate")
                UserDefaults.standard.synchronize()
            }
            completed()
        }
    }
    
    func downloadValueFromDate(date:String, completed : @escaping DownloadCompleate) {
        
        let fullURL = "\(HISTORY_EXCHANGE_URL)\(date)"
        let URL = fullURL
        
        var baseCurrency, currency: String!
        var purchaseRate, purchaseRateNB, saleRate, saleRateNB: Double!
        var date: Date!
        
        request(URL).responseJSON { (response) in
            print(response)
            if let JSON = response.result.value as? [String : Any] {
                if let exchangeRate = JSON["exchangeRate"] as? [[String : Any]], exchangeRate.count > 0 {
                    for i in 0..<exchangeRate.count {
                        baseCurrency = nil
                        if let JSONBaseCurrency = exchangeRate[i]["baseCurrency"] as? String {
                            baseCurrency = JSONBaseCurrency
                        }
                        currency = nil
                        if let JSONCurrency = exchangeRate[i]["currency"] as? String {
                            currency = JSONCurrency
                        }
                        purchaseRate = nil
                        if let JSONPurchaseRate = exchangeRate[i]["purchaseRate"] as? Double {
                            purchaseRate = JSONPurchaseRate
                        }
                        purchaseRateNB = nil
                        if let JSONPurchaseRateNB = exchangeRate[i]["purchaseRateNB"] as? Double {
                            purchaseRateNB = JSONPurchaseRateNB
                        }
                        saleRate = nil
                        if let JSONSaleRate = exchangeRate[i]["saleRate"] as? Double {
                            saleRate = JSONSaleRate
                        }
                        saleRateNB = nil
                        if let JSONSaleRateNB = exchangeRate[i]["saleRateNB"] as? Double {
                            saleRateNB = JSONSaleRateNB
                        }
                        date = nil
                        if let JSONDate = JSON["date"] as? String {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd.MM.yyyy"
                            date = dateFormatter.date(from: JSONDate)
                        }
                        
                        let dataHistoryGraphic = DataCurrency.init(date:date as NSDate, baseCurrency: baseCurrency, currency: currency, purchaseRate: purchaseRate, purchaseRateNB: purchaseRateNB, saleRate: saleRate, saleRateNB: saleRateNB)
                        
                        DataBaseManager.instance.storeRateExchange(dateForStore: dataHistoryGraphic)

                    }
                }
            }
            completed()
        }
    }
}


