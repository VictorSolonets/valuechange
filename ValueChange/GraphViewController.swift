//
//  GraphViewController.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit
import Charts

class GraphViewController: UIViewController {

    @IBOutlet weak var SegmentControlCurrency: UISegmentedControl!
    @IBOutlet weak var chartValues: LineChartView!
    
    weak var axisFormatDelegate: IAxisValueFormatter?
    
    var dataHistoryExchanges = [DataCurrency]();
    
    var dollarsPurchaseArray = [Double]();
    var dollarsPurchaseNBArray = [Double]();
    var euroPurchaseArray = [Double]();
    var euroPurchaseNBArray = [Double]();
    
    var dollarsSaleArray = [Double]();
    var dollarsSaleNBArray = [Double]();
    var euroSaleArray = [Double]();
    var euroSaleNBArray = [Double]();
    
    var date = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Statistic"
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeSegment(_ sender: Any) {
        self.setChart()
    }
    
    func reloadData() {
        CustomLoadingHUD.showHUD()
        DataManager.instance.getCurrencyForMonth(completed: {
            self.dataHistoryExchanges = DataManager.instance.resultArray!
            var i = 0
            while i < self.dataHistoryExchanges.count - 1 {
                self.dollarsPurchaseArray.append(self.dataHistoryExchanges[i].purchaseRate)
                self.dollarsPurchaseNBArray.append(self.dataHistoryExchanges[i].purchaseRateNB)
                
                self.euroPurchaseArray.append(self.dataHistoryExchanges[i + 1].purchaseRate)
                self.euroPurchaseNBArray.append(self.dataHistoryExchanges[i + 1].purchaseRateNB)
                
                self.dollarsSaleArray.append(self.dataHistoryExchanges[i].saleRate)
                self.dollarsSaleNBArray.append(self.dataHistoryExchanges[i].saleRateNB)
                
                self.euroSaleArray.append(self.dataHistoryExchanges[i + 1].saleRate)
                self.euroSaleNBArray.append(self.dataHistoryExchanges[i + 1].saleRateNB)
                
                self.date.append(self.dataHistoryExchanges[i].date.timeIntervalSince1970)
                i += 2
            }
            self.setChart()
            CustomLoadingHUD.hideHUD()
        })
    }
    
    func setChart() {
        if self.dollarsPurchaseArray.count > 0 {
            
            axisFormatDelegate = self
            
            var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
            var yVals2 : [ChartDataEntry] = [ChartDataEntry]()
            var yVals3 : [ChartDataEntry] = [ChartDataEntry]()
            var yVals4 : [ChartDataEntry] = [ChartDataEntry]()
            
            for i in 0..<dollarsPurchaseArray.count {
                yVals1.append(ChartDataEntry(x: self.date[i], y : self.SegmentControlCurrency.selectedSegmentIndex == 0 ? dollarsPurchaseArray[i] : dollarsSaleArray[i]))
                yVals2.append(ChartDataEntry(x: self.date[i], y : self.SegmentControlCurrency.selectedSegmentIndex == 0 ? dollarsPurchaseNBArray[i] : dollarsSaleNBArray[i] ))
                yVals3.append(ChartDataEntry(x: self.date[i], y : self.SegmentControlCurrency.selectedSegmentIndex == 0 ? euroPurchaseArray[i] : euroSaleArray[i]))
                yVals4.append(ChartDataEntry(x: self.date[i], y : self.SegmentControlCurrency.selectedSegmentIndex == 0 ? euroPurchaseNBArray[i] : euroSaleNBArray[i]))
            }
            
			let set1: LineChartDataSet = LineChartDataSet(entries: yVals1, label: "USD")
			let set2: LineChartDataSet = LineChartDataSet(entries: yVals2, label: "USD NB")
			let set3: LineChartDataSet = LineChartDataSet(entries: yVals3, label: "EUR")
			let set4: LineChartDataSet = LineChartDataSet(entries: yVals4, label: "EUR NB")
            
            setupLineChart(set: set1, color: UIColor.init(hex: "#228B22"))
            setupLineChart(set: set2, color: UIColor.init(hex: "228B22", alpha: 0.3))
            setupLineChart(set: set3, color: UIColor.init(hex: "#1E90FF"))
            setupLineChart(set: set4, color: UIColor.init(hex: "#1E90FF", alpha: 0.3))
            
            var dataSets : [LineChartDataSet] = [LineChartDataSet]()
            dataSets.append(set1)
            dataSets.append(set2)
            dataSets.append(set3)
            dataSets.append(set4)
            
            let data: LineChartData = LineChartData(dataSets: dataSets)
            data.setValueTextColor(.black)
            
            let xaxis = chartValues.xAxis
            xaxis.valueFormatter = axisFormatDelegate
            
            chartValues.chartDescription?.text = ""
            
            self.chartValues.data = data
        } else {
            DataManager.instance.showAlert(with:"No data to show")
        }
    }
    
    func setupLineChart(set:LineChartDataSet, color:UIColor) {
        set.axisDependency = .left
        set.setColor(color)
        set.setCircleColor(color)
        set.lineWidth = 1.0
        set.circleRadius = 2.0
        set.fillAlpha = 65 / 255.0
        set.fillColor = color
    }
}


