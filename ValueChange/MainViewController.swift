//
//  MainViewController.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate {
    
    var refreshControl : UIRefreshControl!
    @IBOutlet weak var exchangeTableView: UITableView!
    var dataExchanges = [DataCurrency]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.exchangeTableView.delegate = self
        self.exchangeTableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadData), for: UIControlEvents.valueChanged)
        exchangeTableView.insertSubview(refreshControl, at: 0)
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Value Exchange"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
	@objc func reloadData() {
        DataManager.instance.getCurrencyForToday(completed: {
            self.dataExchanges = DataManager.instance.resultArray!
            self.exchangeTableView.reloadData()
            self.refreshControl.endRefreshing()
        })
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerViewsArray = Bundle.main.loadNibNamed("HeaderExchangeView", owner: self, options: nil)
        return headerViewsArray?.first as! UIView?
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataExchanges.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let exchangeInfo = self.dataExchanges[indexPath.row]
		let classStr = String(describing: ExchangeTableViewCell.self)
        let cell:ExchangeTableViewCell = tableView.dequeueReusableCell(withIdentifier: classStr) as! ExchangeTableViewCell!
        
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        cell.baseCCY.text = exchangeInfo.baseCurrency
        cell.exchangerateCCY.text = exchangeInfo.currency
        cell.buyValueLabel.text = nf.string(from: exchangeInfo.saleRate as NSNumber? ?? 0.0 as NSNumber)
        cell.saleValueLabel.text = nf.string(from: exchangeInfo.purchaseRate as NSNumber? ?? 0.0 as NSNumber)
        
        return cell
    }
    
}
