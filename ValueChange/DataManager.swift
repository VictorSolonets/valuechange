//
//  DataManager.swift
//  ValueChange
//
//  Created by VictorSolo on 1/5/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Foundation
import ReachabilitySwift

let kSecondsInOneDay:Int = 86400;

class DataManager {
    
    static let instance = DataManager()
    
    var resultArray:[DataCurrency]? = [DataCurrency]()
    
    private let reachability = Reachability()!
    
    private init() {
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func getCurrencyForToday(completed : @escaping DownloadCompleate) {
        let resultArrayData = UserDefaults.standard.object(forKey: "TodayExchange") as? NSData
        
        let dateInCache = UserDefaults.standard.object(forKey: "TodayDate")
        
        if (dateInCache != nil && ((dateInCache as! NSDate).isEqual(Date.getTimeInterval(from: Date.init()) as NSDate))) {
            let resultArray = NSKeyedUnarchiver.unarchiveObject(with: resultArrayData as! Data) as? [DataCurrency]
            self.resultArray = resultArray
            completed()
        } else if (self.tryInternet()) {
            ExchancheDataServises.instance.downloadInCashValues(inCashValue: false, completed:{
                self.getCurrencyForToday(completed: {
                    completed()
                })
            })
        } else {
            completed()
            self.showAlert(with: "No Internet Connection")
        }
    }

    func getCurrencyForMonth(completed : @escaping DownloadCompleate) {
        var finish:Int = 30
        var lastDay:Date = Date.init();
        
        for daysToAdd in 1..<31 {
            let now = NSDate.init()
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = "dd.MM.yyyy";
            let smth = -kSecondsInOneDay * daysToAdd
            let nextDate:Date = now.addingTimeInterval(TimeInterval(smth)) as Date
            if daysToAdd == 30 {
                lastDay = nextDate
            }
            self.getCurrency(from: nextDate, forGraphic: daysToAdd == 30, forToday: false, completed: {
                finish -= 1
                if finish == 0 {
                    self.resultArray = DataBaseManager.instance.getRateExchange(from: Date.getTimeInterval(from: lastDay) as NSDate, forGraphic: true)
                    completed()
                }
            })
        }
    }
    
    
    func getCurrency(from date:Date, forGraphic:Bool, forToday:Bool, completed : @escaping DownloadCompleate) {
        self.resultArray = DataBaseManager.instance.getRateExchange(from: Date.getTimeInterval(from: date) as NSDate, forGraphic: forGraphic)
        if self.resultArray!.count < 1 {
            if (self.tryInternet()) {
                ExchancheDataServises.instance.downloadValueFromDate(date: String.stringDate(from: date), completed: {
                    self.resultArray = DataBaseManager.instance.getRateExchange(from: Date.getTimeInterval(from: date) as NSDate, forGraphic: forGraphic)
                    completed()
                })
            } else {
                completed()
                self.showAlert(with: "No Internet Connection")
            }
        } else {
            completed()
        }
    }
    
    func tryInternet() -> Bool {
        return reachability.isReachable
    }
    
    func showAlert(with text:String) {
        let alertController = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
