//
//  ExchangeHistryTableViewCell.swift
//  ValueChange
//
//  Created by VictorSolo on 12/19/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

class ExchangeHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var currency: UILabel!
    @IBOutlet weak var purchaseRate: UILabel!
    @IBOutlet weak var purchaseRateNB: UILabel!
    @IBOutlet weak var saleRate: UILabel!
    @IBOutlet weak var saleRateNB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
