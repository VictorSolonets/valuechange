//
//  RateExchange+CoreDataProperties.swift
//  ValueChange
//
//  Created by VictorSolo on 1/16/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Foundation
import CoreData


extension RateExchange {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RateExchange> {
        return NSFetchRequest<RateExchange>(entityName: "RateExchange");
    }

    @NSManaged public var baseCurrency: String?
    @NSManaged public var currency: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var purchaseRate: Double
    @NSManaged public var purchaseRateNB: Double
    @NSManaged public var saleRate: Double
    @NSManaged public var saleRateNB: Double
    @NSManaged public var isToday: Bool

}
