//
//  DataForGraphic.swift
//  ValueChange
//
//  Created by VictorSolo on 12/20/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation

class DataCurrency : NSObject, NSCoding {
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.baseCurrency, forKey: "baseCurrency")
        coder.encode(self.currency, forKey: "currency")
        coder.encode(self.purchaseRate, forKey: "purchaseRate")
        coder.encode(self.date.timeIntervalSinceNow, forKey: "date")
        coder.encode(self.purchaseRateNB, forKey: "purchaseRateNB")
        coder.encode(self.saleRate, forKey: "saleRate")
        coder.encode(self.saleRateNB, forKey: "saleRateNB")
    }
    
    var date: NSDate!
    var baseCurrency: String!
    var currency: String!
    var purchaseRate: Double!
    var purchaseRateNB: Double!
    var saleRate: Double!
    var saleRateNB: Double!
    
    required init(coder decoder: NSCoder) {
        self.baseCurrency = decoder.decodeObject(forKey: "baseCurrency") as! String
        self.currency = decoder.decodeObject(forKey: "currency") as! String
        self.purchaseRate = decoder.decodeObject(forKey: "purchaseRate") as! Double
        self.currency = decoder.decodeObject(forKey: "currency") as? String
        self.purchaseRateNB = decoder.decodeObject(forKey: "purchaseRateNB") as? Double
        self.saleRate = decoder.decodeObject(forKey: "saleRate") as? Double
        self.saleRateNB = decoder.decodeObject(forKey: "saleRateNB") as? Double
    }
    
    init (date: NSDate, baseCurrency: String?, currency: String?, purchaseRate: Double?, purchaseRateNB: Double?, saleRate: Double?, saleRateNB: Double?) {
        self.date = date
        self.baseCurrency = baseCurrency
        self.currency = currency
        self.purchaseRate = purchaseRate
        self.purchaseRateNB = purchaseRateNB
        self.saleRate = saleRate
        self.saleRateNB = saleRateNB
    }
}
