//
//  CustomLoadingHUD.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation
import UIKit

class CustomLoadingHUD:UIActivityIndicatorView {
    
    static private var indicatorView:UIActivityIndicatorView!
    
    static func showHUD() {
        DispatchQueue.main.async {
            (UIApplication.shared.keyWindow?.rootViewController?.view)!.isUserInteractionEnabled = false
            indicatorView = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
            indicatorView.color = UIColor.darkGray
            indicatorView.center = (UIApplication.shared.keyWindow?.rootViewController?.view)!.center
            indicatorView.startAnimating()
            (UIApplication.shared.keyWindow?.rootViewController?.view)!.addSubview(indicatorView)
        }
    }
    
    static func hideHUD() {
        DispatchQueue.main.async {
            (UIApplication.shared.keyWindow?.rootViewController?.view)!.isUserInteractionEnabled = true
            indicatorView.removeFromSuperview()
        }
    }
    
    static func isIndicatorOnTop() -> Bool {
        for view in (UIApplication.shared.keyWindow?.rootViewController?.view)!.subviews {
            if view.isKind(of: self.superclass()!) {
                return true
            }
        }
        return false
    }
    
}
