//
//  ExchangeTableViewCell.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

class ExchangeTableViewCell: UITableViewCell {

    @IBOutlet var baseCCY: UILabel!
    @IBOutlet var exchangerateCCY: UILabel!
    @IBOutlet var saleValueLabel: UILabel!
    @IBOutlet var buyValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
