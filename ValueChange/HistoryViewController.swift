//
//  HistoryViewController.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

let kShowConstraintsDatePicker:CGFloat = -30
let kHideConstraintsDatePicker:CGFloat = -330

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var exchangeHistoryTableView: UITableView!
    @IBOutlet weak var dateToShowButton: UIButton!
    @IBOutlet weak var datePickerConstraints: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var refreshControl : UIRefreshControl!
    var dataHistoryExchanges = [DataCurrency]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recognizer = UITapGestureRecognizer.init(target: self, action: #selector(cancelAction))
        self.view.addGestureRecognizer(recognizer)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadData), for: UIControlEvents.valueChanged)
        exchangeHistoryTableView.insertSubview(refreshControl, at: 0)
        setupDatePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Detail"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    func setupDatePicker() {
        self.datePicker.datePickerMode = .date
        self.datePicker.maximumDate = Date.init()
        let dateComponents = NSDateComponents()
        dateComponents.year = -8
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: .gregorian)!
        calendar.timeZone = TimeZone.init(identifier: "UTC")!
        let minDate = calendar.date(byAdding: dateComponents as DateComponents, to: Date.init(), options:  NSCalendar.Options(rawValue: 0))
        self.datePicker.minimumDate = minDate
        updateDatePicker()
        self.doneAction(calendar)
    }
    
    func updateDatePicker() {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "dd.MM.YYYY"
        self.dateToShowButton.setTitle(dateFormatter.string(from: self.datePicker.date), for: .normal)
    }
    
    @IBAction func showDatePicker(_ sender: Any) {
        self.datePickerConstraints.constant = kShowConstraintsDatePicker
        self.animateConstraints()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        updateDatePicker()
        self.datePickerConstraints.constant = kHideConstraintsDatePicker
        self.animateConstraints()
        CustomLoadingHUD.showHUD()
        reloadData()
    }
    
    @IBAction func cancelAction() {
        self.datePickerConstraints.constant = kHideConstraintsDatePicker
        self.animateConstraints()
    }
    
    func animateConstraints() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func reloadData() {
        DataManager.instance.getCurrency(from: self.datePicker.date, forGraphic: false, forToday: false, completed: {
            self.dataHistoryExchanges = DataManager.instance.resultArray!
            self.exchangeHistoryTableView.reloadData()
            self.refreshControl.endRefreshing()
            CustomLoadingHUD.hideHUD()
        })
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerViewsArray = Bundle.main.loadNibNamed("HeaderExchangeView", owner: self, options: nil)
        return headerViewsArray?.first as! UIView?
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataHistoryExchanges.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let exchangeInfo = self.dataHistoryExchanges[indexPath.row]
        let cell:ExchangeHistoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ExchangeHistoryTableViewCell") as! ExchangeHistoryTableViewCell!
        
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        cell.currency.text = exchangeInfo.currency
        cell.purchaseRate.text = nf.string(from: exchangeInfo.purchaseRate as NSNumber? ?? 0.0 as NSNumber)
        cell.purchaseRateNB.text = nf.string(from: exchangeInfo.purchaseRateNB as NSNumber? ?? 0.0 as NSNumber)
        cell.saleRate.text = nf.string(from: exchangeInfo.saleRate as NSNumber? ?? 0.0 as NSNumber)
        cell.saleRateNB.text = nf.string(from: exchangeInfo.saleRateNB as NSNumber? ?? 0.0 as NSNumber)
        return cell
    }
}
