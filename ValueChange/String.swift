//
//  String.swift
//  ValueChange
//
//  Created by VictorSolo on 1/4/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Foundation

extension String {
    
    static func stringDate(from date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: date)
    }
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }
}
