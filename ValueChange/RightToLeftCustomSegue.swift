//
//  RightToLeftCustomTransition.swift
//  ValueChange
//
//  Created by VictorSolo on 1/16/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import UIKit

class RightToLeftCustomSegue: UIStoryboardSegue {
    
    override func perform() {
        let sourceViewController = self.source
        let destinationViewController = self.destination
        
        sourceViewController.view.addSubview(destinationViewController.view)
        
        destinationViewController.view.frame.origin.x = -destinationViewController.view.frame.size.width
        
        UIView.animate(withDuration: 0.3, animations: { 
            destinationViewController.view.frame = sourceViewController.view.frame
        }) { (completed) in
            if completed {
                destinationViewController.view.removeFromSuperview()
                sourceViewController.navigationController?.pushViewController(destinationViewController, animated: false)
            }
        }
    }
}
