//
//  Constants.swift
//  ValueChange
//
//  Created by VictorSolo on 12/18/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation

let HISTORY_EXCHANGE_URL = "https://api.privatbank.ua/p24api/exchange_rates?json&date="
//безналичний курс
let IN_CASH_VALUE_CHANGE_URL = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5"
//наличний курс
let NON_CASH_VALUE_CHANGE_URL =  "https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11"

typealias DownloadCompleate = () -> ()
