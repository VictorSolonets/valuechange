//
//  IAxisValueFormatter.swift
//  ValueChange
//
//  Created by VictorSolo on 1/3/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Foundation
import UIKit
import Charts

extension UIViewController: IAxisValueFormatter {

    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM"
        return dateFormatter.string(from: Date(timeIntervalSince1970: value))
    }
}
