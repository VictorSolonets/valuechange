//
//  TodayViewController.swift
//  ValueChangeHome
//
//  Created by VictorSolo on 6/19/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import UIKit
import Pods_ValueChange
import NotificationCenter
import Foundation


class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet var exchangeTableView: UITableView!
    
    var dataExchanges = [DataCurrency]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Value Exchange"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    func reloadData() {
        
        let resultArrayData = UserDefaults.standard.object(forKey: "TodayExchange") as? NSData
        let dateInCache = UserDefaults.standard.object(forKey: "TodayDate")
        
        if (dateInCache != nil && ((dateInCache as! NSDate).isEqual(NSDate()))) {
            let resultArray = NSKeyedUnarchiver.unarchiveObject(with: resultArrayData! as Data) as? [DataCurrency]
            self.dataExchanges = resultArray!
            self.exchangeTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerViewsArray = Bundle.main.loadNibNamed("HeaderExchangeView", owner: self, options: nil)
        return headerViewsArray?.first as! UIView?
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataExchanges.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let exchangeInfo = self.dataExchanges[indexPath.row]
        let cell:ExchangeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ExchangeTableViewCell") as! ExchangeTableViewCell!
        
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        cell.baseCCY.text = exchangeInfo.baseCurrency
        cell.exchangerateCCY.text = exchangeInfo.currency
        cell.buyValueLabel.text = nf.string(from: exchangeInfo.saleRate as NSNumber? ?? 0.0 as NSNumber)
        cell.saleValueLabel.text = nf.string(from: exchangeInfo.purchaseRate as NSNumber? ?? 0.0 as NSNumber)
        
        return cell
    }
}
